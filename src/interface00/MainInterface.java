package interface00;

import java.util.Scanner;
import javax.swing.JFrame;

public class MainInterface {

    public static void main(String[] args) {
        Scanner read= new Scanner(System.in); 
        MyInterface leo = new Person();
        ((Person)leo).setName("Leo");
        
        MyInterface desktop = new Form();
        //show the jFrame
        ((JFrame)desktop).setVisible(true);
        
        System.out.print("Digit message to send to object:");
        String Msg= read.nextLine();
        
        //Invoking methods from interface
        leo.showMessage(Msg);
        desktop.showMessage(Msg);
        
        System.out.println("Object user:"+leo.getUser());
        System.out.println("Desktop user:"+desktop.getUser());
    }
}
